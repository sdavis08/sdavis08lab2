package cs.mad.flashcards.entities

data class Flashcard(val term: String = "", var answer: String = "") {
    fun setmaker(): MutableList<Flashcard> {
        val cards = mutableListOf<Flashcard>()
        cards.add(0, Flashcard(term = "Erroneous", answer ="Wrong; Incorrect"))
        cards.add(1, Flashcard("Water", "H2O"))
        cards.add(2, Flashcard("Skeleton", "The bones or skeletal system of any organism with bones. The complete set."))
        cards.add(3, Flashcard("CPU", "The Central Processing Unit. It is like the heart of the computer."))
        cards.add(4, Flashcard("Scotch", "The best drink to have."))
        cards.add(5, Flashcard("Colt 45", "The best weapon to be created in existence."))
        cards.add(6, Flashcard("One Piece", "The best book and show in existence."))
        cards.add(7, Flashcard("UAB", "Best school in the nation."))
        cards.add(8, Flashcard("Flamboyant", "Tending to attract attention because of their exuberance, confidence, and stylishness."))
        cards.add(9, Flashcard("Realtor", "A person who acts as an agent for the sale and purchase of building and land."))

        return cards
    }


}




