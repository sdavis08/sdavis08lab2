package cs.mad.flashcards.entities

data class Flashcardset(val title: String  = "") {
    fun setmaker(): MutableList<Flashcardset> {
        val cards = mutableListOf<Flashcardset>()
        cards.add(0, Flashcardset(title = "Vocabulary Words"))
        cards.add(1, Flashcardset(title = "Times Table"))
        cards.add(2, Flashcardset(title = "Elements"))
        cards.add(3, Flashcardset(title = "Herbivores"))
        cards.add(4, Flashcardset(title = "Carnivores"))
        cards.add(5, Flashcardset(title = "Mammals"))
        cards.add(6, Flashcardset(title = "Fish"))
        cards.add(7, Flashcardset(title = "Birds"))
        cards.add(8, Flashcardset(title = "Reptiles"))
        cards.add(9, Flashcardset(title = "Amphibians"))

        return cards
    }


}



